# people_body_parts_segments 

Нейросеть для разбиение человека на части и получения масок отдельных сегментов человека.
ДЛя запуска необходимо запустить файл: 
### apply_net_get_segments.py
### Зависимости для сборки проекта :
 - detectron2 (https://github.com/facebookresearch/detectron2)
 - Linux or macOS with Python ≥ 3.6
 - PyTorch ≥ 1.8 and torchvision that matches the PyTorch installation. Install them together at pytorch.org to make sure of this
 - OpenCV is optional but needed by demo and visualization
